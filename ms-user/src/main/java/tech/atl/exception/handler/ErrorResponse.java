package tech.atl.exception.handler;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ErrorResponse {
    String message;

    public static ErrorResponse of(Exception e) {
        return new ErrorResponse(e.getMessage());
    }

    public static ErrorResponse of(String errorMessage) {
        return new ErrorResponse(errorMessage);
    }




}
