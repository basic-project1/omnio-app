package tech.atl.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import tech.atl.exception.InvalidPasswordException;
import tech.atl.exception.UserNotFound;

@RestController
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(UserNotFound.class)
    public ErrorResponse handle(UserNotFound exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }
    @ExceptionHandler(InvalidPasswordException.class)
    public ErrorResponse handle(InvalidPasswordException exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }


}

