package tech.atl.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tech.atl.dao.entity.User;
import tech.atl.model.dto.UpdatePassRequest;
import tech.atl.model.dto.UserDto;
import tech.atl.service.ProfileService;

import java.util.List;


@RestController
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {
    private final ProfileService profileService;

    @GetMapping("/get-user-by-id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUserProfile(@PathVariable Long id) {
        return profileService.getUserById(id);

    }

    @GetMapping("/get-all-users")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsers() {
        return profileService.getAllUsers();

    }

    @PutMapping("/update-user/{id}")
    public UserDto updateUser(@PathVariable Long id, @RequestBody User user) {
        return profileService.updateUser(id, user);

    }

    @PutMapping("/update-pass/{id}")
    public void updatePassword(@PathVariable Long id, @RequestBody UpdatePassRequest updatePassRequest) {
        profileService.updatePassword(id, updatePassRequest);
    }

    @DeleteMapping("/delete-user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable Long id) {
        profileService.deleteById(id);
    }

    @GetMapping("/get-users-with-pagination/{offset}/{pageSize}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<UserDto> getUsersWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
        return profileService.getWithPagination(offset, pageSize);


    }


}
