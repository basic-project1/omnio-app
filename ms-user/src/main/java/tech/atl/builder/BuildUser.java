package tech.atl.builder;

import org.bouncycastle.LICENSE;
import tech.atl.dao.entity.User;
import tech.atl.model.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

public enum BuildUser {
    BUILD_USER;

    public final UserDto buildDto(User user) {
        return UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .email(user.getEmail())
                .jobTitle(user.getJobTitle())
                .role(user.getRole())
                .build();
    }

    public final List<UserDto> buildDtoList(List<User> userList) {
        List<UserDto> userDtoList = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            userDtoList.add(buildDto(userList.get(i)));
        }
        return userDtoList;

    }
}
