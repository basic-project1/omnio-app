package tech.atl.annotation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class CheckEmailValidator implements ConstraintValidator<CheckEmail, String> {

    @Override
    public void initialize(CheckEmail email) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        String regex1 = "^[a-zA-Z0-9._%+-]+@gmail\\.com$";
        String regex2 = "^[a-zA-Z0-9._%+-]+@mail\\.ru$";
        return email.matches(regex1) || email.matches(regex2);
    }
}
