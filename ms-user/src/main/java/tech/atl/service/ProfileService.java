package tech.atl.service;

import tech.atl.dao.entity.User;
import tech.atl.model.dto.UpdatePassRequest;
import tech.atl.model.dto.UserDto;

import java.util.List;

public interface ProfileService {
    UserDto getUserById(Long id);
    List<UserDto> getAllUsers();
    UserDto updateUser(Long id, User user);
    void updatePassword(Long id, UpdatePassRequest updatePassRequest);
    void deleteById(Long id);
    List<UserDto> getWithPagination(int offset, int pageSize);
}
