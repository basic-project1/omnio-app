package tech.atl.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tech.atl.dao.entity.User;
import tech.atl.dao.repository.UserRepository;
import tech.atl.exception.InvalidPasswordException;
import tech.atl.exception.UserNotFound;

import static tech.atl.model.consts.OperationMessage.INVALID_PASSWORD;
import static tech.atl.model.consts.OperationMessage.USER_NOT_FOUND;

import tech.atl.model.dto.UpdatePassRequest;
import tech.atl.model.dto.UserDto;
import tech.atl.service.ProfileService;

import java.util.List;
import java.util.Locale;

import static tech.atl.builder.BuildUser.BUILD_USER;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final MessageSource messageSource;

    @Override
    public UserDto getUserById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] objs1 = new Object[1];
        objs1[0] = id;
        return BUILD_USER.buildDto(userRepository.findById(id)
                .orElseThrow(() ->
                        new UserNotFound(messageSource.getMessage(USER_NOT_FOUND.getMessage(), objs1, locale))));


    }

    @Override
    public List<UserDto> getAllUsers() {
        return BUILD_USER.buildDtoList(userRepository.findAll());
    }

    @Override
    public UserDto updateUser(Long id, User user) {
        User userFromDb = userRepository.findById(id).orElseThrow(
                ()->new UserNotFound("User with this " + id + "not found"));
        userFromDb.setName(user.getName());
        userFromDb.setSurname(user.getSurname());
        userFromDb.setUsername(user.getUsername());
        userFromDb.setEmail(user.getEmail());
        userFromDb.setJobTitle(user.getJobTitle());
        userFromDb.setRole(user.getRole());
        userRepository.save(userFromDb);
        return BUILD_USER.buildDto(userFromDb);

    }

    @Override
    public void updatePassword(Long id, UpdatePassRequest updatePassRequest) {
        Locale locale = LocaleContextHolder.getLocale();
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFound("User with this " + id + " is not found"));
        if (passwordEncoder.matches(updatePassRequest.getCurrentPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(updatePassRequest.getNewPassword()));
            userRepository.save(user);
        } else {
            throw new InvalidPasswordException(messageSource.getMessage(INVALID_PASSWORD.getMessage(), null, locale));

        }


    }

    @Override
    public void deleteById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] objs1 = new Object[1];
        objs1[0] = id;
        userRepository.findById(id).orElseThrow(
                () -> new UserNotFound(messageSource.getMessage(USER_NOT_FOUND.getMessage(), objs1, locale)));
        userRepository.deleteById(id);
    }

    @Override
    public List<UserDto> getWithPagination(int offset, int pageSize) {
        return BUILD_USER.buildDtoList(userRepository.findAll(PageRequest.of(offset, pageSize)).toList());
    }
}
