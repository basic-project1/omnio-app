package tech.atl.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;
import tech.atl.annotation.CheckEmail;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class RegisterRequest {
    @NotBlank
    String name;
    @NotBlank
    String surname;
    String username;
    @CheckEmail
    String email;
    String jobTitle;

    String password;
    Role role;

}
