package tech.atl.builder;

import org.junit.jupiter.api.Test;
import tech.atl.dao.entity.User;
import tech.atl.model.Role;
import tech.atl.model.dto.UserDto;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static tech.atl.builder.BuildUser.BUILD_USER;

public class BuildUserTest {
    @Test
    public void test_buildUserDto() {
        User userEntity = User.builder()
                .name("Ramin")
                .surname("Ibrahimli")
                .username("Ramoneal134")
                .email("raminibra99@gmail.com")
                .jobTitle("Software engineer")
                .password("12345")
                .role(Role.ADMIN)
                .build();
        UserDto userDto = BUILD_USER.buildDto(userEntity);
        assertEquals(userEntity.getName(), userDto.getName());
        assertEquals(userEntity.getRole(), userDto.getRole());
    }

    @Test
    public void test_buildUserDtoList() {
        List<User> userEntityList = Arrays.asList(
                new User(1l, "Ramin", "Ibrahimli", "Ramoneal134",
                        "raminibra99@gmail.com", "Software Engineer", "12345", Role.USER),
                new User(2L, "Narmin", "Ibrahimova", "Nara134", "naraibragim@gmail.com",
                        "Marketing specialist", "123", Role.ADMIN)
        );
        List<UserDto> userDtoList = BUILD_USER.buildDtoList(userEntityList);
        assertEquals(userEntityList.size(),userDtoList.size());
        assertEquals(userEntityList.get(0).getName(),userDtoList.get(0).getName());
    }

}
