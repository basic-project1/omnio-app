package tech.atl.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import tech.atl.dao.entity.User;
import tech.atl.dao.repository.UserRepository;
import tech.atl.exception.UserNotFound;
import tech.atl.model.Role;
import tech.atl.model.dto.UserDto;
import tech.atl.service.impl.ProfileServiceImpl;

import java.util.Arrays;
import java.util.List;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ProfilServiceImplTest {
    @InjectMocks
    private ProfileServiceImpl profileService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private MessageSource messageSource;

    @Test
    public void test_getUserByExistingId() {
        Long userId = 1L;
        User userEntity = User.builder()
                .id(1L)
                .name("Ramin")
                .surname("Ibrahimli")
                .username("Ramoneal134")
                .email("raminibra99@gmail.com")
                .jobTitle("Software engineer")
                .password("Angier12345")
                .role(Role.ADMIN)
                .build();
        when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        UserDto userDto = profileService.getUserById(userId);
        assertNotNull(userDto);
        assertEquals("Ramin", userDto.getName());
        assertEquals("ADMIN", userDto.getRole().toString());
        verify(userRepository, times(1)).findById(userId);


    }

    @Test
    public void test_getUserByNonExistingId() {
        Long id = 1L;
        when(userRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(UserNotFound.class,()->profileService.getUserById(id));
        verify(userRepository,times(1)).findById(id);


    }
    @Test
    public void test_getAllUsers(){
        List<User> userEntityList = Arrays.asList(
               new User(1L,"Ramin","Ibrahimli","Ramoneal134","raminibra99@gmail.com"
               ,"Software engineer","Angier12345",Role.ADMIN),
               new User(2L,"Narmin","Ibrahimova","Nara134","nara1999@gmail.com"
                       ,"Marketing specialist","Nara134",Role.USER)
        );
        when(userRepository.findAll()).thenReturn(userEntityList);
        List<UserDto> userDtoList = profileService.getAllUsers();
        assertNotNull(userDtoList);
        assertEquals(userEntityList.size(),userDtoList.size());
        assertEquals(userEntityList.get(0).getName(),userDtoList.get(0).getName());
        verify(userRepository,times(1)).findAll();
    }

}
